package world.expand.bank.service

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.toast
import world.expand.bank.BankActivity
import world.expand.bank.ui.MainUi

object UserService {
    fun doLogin(act: BankActivity, email: String, password: String, rememberMe: Boolean) {
        act.setLoading(true)
        FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnSuccessListener {
                    Log.i("User", "Logged in successfully!")
                    act.toast("Logged in successfully!")
                    storeOrClearUserDetails(act, rememberMe, email)
                }.addOnFailureListener {task ->
                    Log.e("User", "Failed to log in! ${task.message}")
                    act.toast("Failed to log in...")
                    act.setLoading(false)
                }
    }

    fun storeOrClearUserDetails(act: BankActivity, rememberMe: Boolean, email: String?) {
        act.prefs!!.edit()
                .putBoolean(MainUi.rememberMePref, rememberMe)
                .putString(MainUi.emailPref, if (rememberMe) email else null)
                .apply()
    }
}