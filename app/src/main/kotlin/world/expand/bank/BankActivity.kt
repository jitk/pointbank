package world.expand.bank

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.withArguments
import world.expand.bank.model.User
import world.expand.bank.dialog.AccountDetailsFragment
import world.expand.bank.event.AccountDetailEvent
import world.expand.bank.event.LoginEvent
import world.expand.bank.page.AccountListFragment
import world.expand.bank.page.LoginFragment
import world.expand.bank.ui.MainUi

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class BankActivity : AppCompatActivity() {
    val PREF_NAME = "world.expand.bank.prefs"
    var prefs: SharedPreferences? = null
    var isUserLoggedIn = false

    private var ui: MainUi? = null
    val authListener = FirebaseAuth.AuthStateListener {auth ->
        Log.i("BANK", "Auth state changed. User is now: ${auth.currentUser?.uid}")
        if (auth.currentUser != null) {
            notifyLoggedIn(false)
        } else {
            notifyLoggedOut()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = this.getSharedPreferences(PREF_NAME, 0)

        ui = MainUi(supportFragmentManager)
        ui!!.setContentView(this)

        FirebaseAuth.getInstance().addAuthStateListener(authListener)
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        FirebaseAuth.getInstance().removeAuthStateListener(authListener)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
        FirebaseAuth.getInstance().addAuthStateListener(authListener)
    }

    fun removeTab(title: String) {
        ui!!.removePage(title)
    }

    fun addTab(fragment: Fragment, title: String, position: Int = -1, scrollTo: Boolean = false) {
        ui!!.addPage(fragment, title, position, scrollTo)
    }

    fun notifyLoggedIn(cancelAnimation: Boolean = true) {
        if (isUserLoggedIn) return
        EventBus.getDefault().post(LoginEvent(User(FirebaseAuth.getInstance().currentUser!!.uid)))

        setLogInState(true)

        addTab(AccountListFragment(), "Accounts", scrollTo = true)
        removeTab("Login")

        if (cancelAnimation) setLoading(false)
    }

    fun notifyLoggedOut() {
        if (!isUserLoggedIn) return

        setLogInState(false)

        addTab(LoginFragment(), "Login", scrollTo = true)
        removeTab("Accounts")

        setLoading(false)
    }

    private fun setLogInState(isLoggedIn: Boolean) {
        isUserLoggedIn = isLoggedIn
        invalidateOptionsMenu() // invalidate the menu so it now shows/hides the login button
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        if (isUserLoggedIn) {
            menu.add(0, Menu.FIRST, Menu.NONE, R.string.logoutButton).setIcon(android.R.drawable.ic_lock_power_off)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        if (item.itemId == Menu.FIRST) {
            FirebaseAuth.getInstance().signOut()
        }
        return false
    }

    fun setLoading(loading: Boolean) {
        if (loading) {
            ui!!.animateOverlay()
        } else {
            ui!!.cancelAnimation()
        }
    }

    @Subscribe
    fun handleDetailSelection(event: AccountDetailEvent) {
        Log.i("Detail", "Showing details view for ${event.account.number}")
        AccountDetailsFragment()
                .withArguments("account" to event.account)
                .show(fragmentManager, "accountDetails")
    }
}
