package world.expand.bank.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.InputType
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.viewPager
import world.expand.bank.BankActivity
import world.expand.bank.R
import world.expand.bank.page.LoginFragment

class MainUi(val fm: FragmentManager) : AnkoComponent<BankActivity>, AnkoLogger {
    lateinit private var pagerAdapter: MainPagerAdapter
    lateinit private var viewPager: ViewPager
    lateinit private var tabs: TabLayout
    lateinit private var overlayPane: RelativeLayout
    lateinit private var wheelImage: ImageView

    private var animatorSet = AnimatorSet()

    override fun createView(ui: AnkoContext<BankActivity>) = with(ui) {
        pagerAdapter = MainPagerAdapter(fm)
        frameLayout {
            coordinatorLayout {
                appBarLayout {
                    tabs = tabLayout(R.style.Bank_TabLayoutStyle) {
                        id = R.id.viewPagerTabs
                        tabMode = TabLayout.MODE_FIXED
                        tabGravity = TabLayout.GRAVITY_FILL
                    }.lparams {
                        width = matchParent
                        scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
                    }
                }.lparams { width = matchParent }

                viewPager = viewPager {
                    id = R.id.viewPager
                    adapter = pagerAdapter
                }.lparams {
                    width = matchParent
                    behavior = AppBarLayout.ScrollingViewBehavior()
                }

                tabs.setupWithViewPager(viewPager)
            }
            overlayPane = relativeLayout {
                id = R.id.overlay
                backgroundColor = 0x7f000000
                horizontalPadding = dip(40)
                wheelImage = imageView(imageResource = R.mipmap.wheel).lparams {
                    centerInParent()
                }
            }.lparams {
                width = matchParent
                height = matchParent
            }

            animateOverlay(0)
        }
    }

    fun removePage(title: String) {
        val idx = pagerAdapter.titles.indexOf(title)
        if (idx != -1) {
            pagerAdapter.pages.removeAt(idx)
            pagerAdapter.titles.removeAt(idx)
            pagerAdapter.notifyDataSetChanged()
        }
    }

    fun addPage(fragment: Fragment, title: String, position: Int, scrollTo: Boolean = false) {
        if (pagerAdapter.pages.find {p -> p.javaClass == fragment.javaClass } != null) {
            return
        }

        var adjustedPosition = position
        if (position < 0 || position > pagerAdapter.pages.size) {
            adjustedPosition = pagerAdapter.pages.size
        }

        pagerAdapter.pages.add(adjustedPosition, fragment)
        pagerAdapter.titles.add(adjustedPosition, title)

        if (scrollTo) {
            pagerAdapter.notifyDataSetChanged()
            viewPager.setCurrentItem(adjustedPosition, true)
        }
    }

    fun animateOverlay(repeatCount: Int = -1) {
        animatorSet = AnimatorSet()
        val fadeInAnimator = ObjectAnimator.ofFloat(overlayPane, "alpha", 0f, 1f)
                .setDuration(200)
        val imageAnimator = ObjectAnimator.ofFloat(wheelImage, "rotation", 0f, 1080f)
                .setDuration(3000)
        imageAnimator.repeatCount = repeatCount
        imageAnimator.interpolator = AccelerateDecelerateInterpolator()
        imageAnimator.addListener(object : Animator.AnimatorListener {
            private fun hidePane(duration: Long = 200) {
                overlayPane.animate().alpha(0f).setDuration(duration)
                        .withEndAction {wheelImage.rotation = 0f}
                        .start()
            }
            override fun onAnimationEnd(p0: Animator?) = hidePane()
            override fun onAnimationCancel(p0: Animator?) = hidePane()
            override fun onAnimationRepeat(p0: Animator?) {}
            override fun onAnimationStart(p0: Animator?) {}
        })
        animatorSet.play(fadeInAnimator).before(imageAnimator)
        animatorSet.start()
    }

    fun cancelAnimation() {
        animatorSet.cancel()
    }

    internal class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        val pages = arrayListOf<Fragment>(LoginFragment())
        val titles = arrayListOf("Login")
        override fun getItem(position: Int): Fragment = pages[position]
        override fun getCount(): Int = pages.size
        override fun getPageTitle(position: Int): CharSequence = titles[position]
        override fun getItemPosition(page: Any?): Int {
            val idx = pages.indexOf(page as Fragment)
            if (idx == -1) return PagerAdapter.POSITION_NONE
            return idx
        }
        override fun getItemId(position: Int) = pages[position].hashCode().toLong()
    }

    companion object {
        val passwordCodeInputType: Int = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        val emailPref: String = "cardNumber"
        val rememberMePref: String = "rememberMe"
    }
}