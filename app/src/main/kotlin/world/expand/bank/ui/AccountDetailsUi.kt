package world.expand.bank.ui

import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import android.widget.TextView
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import world.expand.bank.R
import world.expand.bank.dialog.AccountDetailsFragment
import world.expand.bank.event.AccountDetailEvent
import world.expand.bank.model.AccountTransaction
import world.expand.bank.model.BankAccount
import world.expand.bank.model.DebitCredit
import world.expand.bank.util.Drawables
import world.expand.bank.util.OnSwipeTouchListener
import java.text.NumberFormat

class AccountDetailsUi(transactionList: List<AccountTransaction>) : AnkoComponent<AccountDetailsFragment> {
    val transactionAdapter = TransactionListAdapter(transactionList)
    var account: BankAccount? = null

    override fun createView(ui: AnkoContext<AccountDetailsFragment>) = with(ui) {
        relativeLayout {
            verticalLayout {
                id = R.id.detailsMain

                textView {
                    textSize = 24f
                    text = "Account Details"
                    typeface = Typeface.DEFAULT_BOLD
                    gravity = Gravity.CENTER_HORIZONTAL
                }
                textView {
                    textSize = 20f
                    text = account?.name
                }
                textView {
                    textSize = 20f
                    text = account?.number
                }

                recyclerView {
                    adapter = transactionAdapter
                    layoutManager = LinearLayoutManager(ctx)
                    itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
                    itemAnimator.addDuration = 350
                }.lparams {
                    width = matchParent
                }
            }.lparams {
                width = matchParent
                height = wrapContent
                margin = dip(20)

                gravity = Gravity.TOP
                alignParentTop()
                above(R.id.closeButton)
            }


            imageButton(imageResource = android.R.drawable.ic_menu_close_clear_cancel) {
                id = R.id.closeButton
                backgroundDrawable = Drawables.border(Drawables.circle(dip(50), 0xBABADA.opaque), 0x8080C0.opaque, dip(2))
                onClick {
                    owner.dismiss()
                }
            }.lparams {
                width = dip(50)
                height = dip(50)
                margin = dip(20)

                centerHorizontally()
                alignParentBottom()
            }

            setOnTouchListener(object: OnSwipeTouchListener(ctx) {
                override fun onSwipeDown() {
                    owner.dismiss()
                }
            })
        }
    }


    class TransactionListAdapter(var transactionList: List<AccountTransaction>) : RecyclerView.Adapter<TransactionItemViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int)
                = TransactionItemViewHolder(TransactionItemUi().createView(AnkoContext.create(parent!!.context, parent)))

        override fun onBindViewHolder(holder: TransactionItemViewHolder?, position: Int) {
            val transaction = transactionList[position]
            holder!!.bind(transaction)
        }

        override fun getItemCount() = transactionList.size
    }

    class TransactionItemUi : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
            linearLayout {
                lparams {
                    width = matchParent
                    horizontalPadding = dip(10)
                    topPadding = dip(5)
                }

                cardView {
                    id = R.id.accountCard
                    elevation = 8f

                }.lparams {
                    width = matchParent
                    minimumHeight = dip(48)
                    margin = dip(4)
                    clipToPadding = false
                }
            }
        }
    }

    class TransactionItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val amount: TextView = itemView.find(R.id.transactionAmount)
        val debitCredit: TextView = itemView.find(R.id.transactionDebitCredit)
        val fromAccount: TextView = itemView.find(R.id.transactionFromAccount)
        val toAccount: TextView = itemView.find(R.id.transactionToAccount)

        val numberFormat: NumberFormat = NumberFormat.getCurrencyInstance()

        fun bind(transaction: AccountTransaction) {
            amount.text = numberFormat.format(transaction.amount)
            debitCredit.text = if (transaction.debitCredit == DebitCredit.CREDIT) "CR" else "DR"
            fromAccount.text = transaction.fromAccountNumber
            toAccount.text = transaction.toAccountNumber
        }
    }
}