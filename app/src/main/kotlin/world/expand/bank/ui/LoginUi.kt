package world.expand.bank.ui

import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.act
import world.expand.bank.BankActivity
import world.expand.bank.R
import world.expand.bank.page.LoginFragment
import world.expand.bank.service.UserService
import world.expand.bank.util.UiHelper.styleButton
import world.expand.bank.util.UiHelper.styleText

class LoginUi : AnkoComponent<LoginFragment> {
    override fun createView(ui: AnkoContext<LoginFragment>) = with(ui) {
        relativeLayout {
            verticalLayout {
                clipToPadding = false
                padding = dip(20)

                val email = editText {
                    id = R.id.email
                    hintResource = R.string.cardPlaceholder
                    singleLine = true
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    setText(owner.prefs?.getString(MainUi.emailPref, ""))
                }
                val password = editText {
                    id = R.id.password
                    hintResource = R.string.passwordPlaceholder
                    inputType = MainUi.passwordCodeInputType
                }
                val rememberMe = checkBox(R.style.BankCheckBoxStyle) {
                    id = R.id.rememberMe
                    textResource = R.string.rememberMe
                    isChecked = owner.prefs?.getBoolean(MainUi.rememberMePref, false) ?: false
                    onClick {
                        if (!isChecked) {
                            // clear stored details as soon as checkbox is unticked
                            UserService.storeOrClearUserDetails(owner.act as BankActivity, false, null)
                        }
                    }
                }

                button(R.style.Bank_ButtonStyle) {
                    id = R.id.loginButton
                    textResource = R.string.loginButton
                    onClick {
                        owner.loginWithFirebase(email.text?.toString(), password.text?.toString(), rememberMe.isChecked)
                    }
                    styleButton(20f, ContextCompat.getColor(ctx, R.color.buttonAccent), 0)
                }
            }.lparams {
                topMargin = dip(40)
                width = matchParent
                horizontalMargin = dip(20)
            }
        }.applyRecursively(customStyle)
    }

    private val customStyle = { v: View ->
        when (v) {
            is EditText -> {
                v.styleText(18f, ContextCompat.getColor(v.context, R.color.textColour),
                        ContextCompat.getColor(v.context, R.color.textPlaceholder))
                v.typeface = Typeface.DEFAULT //enforce standard font for password fields
            }
            is CheckBox -> {
                v.textSize = 18f
                v.textColor = ContextCompat.getColor(v.context, R.color.textPlaceholder)
            }
        }
    }
}