package world.expand.bank.ui

import android.graphics.Typeface
import android.support.v7.widget.CardView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import android.widget.TextView
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import world.expand.bank.BankActivity
import world.expand.bank.R
import world.expand.bank.event.AccountDetailEvent
import world.expand.bank.model.BankAccount
import world.expand.bank.page.AccountListFragment

class AccountListUi(accountList: List<BankAccount>) : AnkoComponent<AccountListFragment> {
    val accountAdapter = AccountListAdapter(accountList)

    override fun createView(ui: AnkoContext<AccountListFragment>) = with(ui) {
        relativeLayout {
            recyclerView {
                adapter = accountAdapter
                layoutManager = LinearLayoutManager(ctx)
                itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
                itemAnimator.addDuration = 500
            }.lparams {
                width = matchParent
            }
        }
    }

    class AccountListAdapter(var accountList: List<BankAccount>) : RecyclerView.Adapter<AccountListItemViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int)
            = AccountListItemViewHolder(AccountListItemUi().createView(AnkoContext.create(parent!!.context, parent)))

        override fun onBindViewHolder(holder: AccountListItemViewHolder?, position: Int) {
            val account = accountList[position]
            holder!!.bind(account)
        }

        override fun getItemCount() = accountList.size
    }

    class AccountListItemUi : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
            linearLayout {
                lparams {
                    width = matchParent
                    horizontalPadding = dip(10)
                    topPadding = dip(5)
                }

                cardView {
                    id = R.id.accountCard
                    elevation = 8f

                    verticalLayout {
                        textView {
                            id = R.id.accountName
                            textSize = 18f
                            textAlignment = View.TEXT_ALIGNMENT_CENTER
                            typeface = Typeface.DEFAULT_BOLD
                        }
                        verticalLayout {
                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    textResource = R.string.accountNumber
                                    typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
                                }.lparams { rightMargin = dip(10); width = dip(120) }

                                textView {
                                    id = R.id.accountNumber
                                }
                            }
                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    textResource = R.string.accountBalance
                                    typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
                                }.lparams { rightMargin = dip(10); width = dip(120) }

                                textView {
                                    id = R.id.accountBalance
                                }
                            }
                        }

                        horizontalPadding = dip(20)
                        verticalPadding = dip(10)
                    }.applyRecursively { v ->
                        when (v) {
                            is TextView -> v.textSize = 16f
                        }
                    }
                }.lparams {
                    width = matchParent
                    minimumHeight = dip(48)
                    margin = dip(4)
                    clipToPadding = false
                }
            }
        }
    }

    class AccountListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val card: CardView = itemView.find(R.id.accountCard)
        val name: TextView = itemView.find(R.id.accountName)
        val number: TextView = itemView.find(R.id.accountNumber)
        val balance: TextView = itemView.find(R.id.accountBalance)

        fun bind(account: BankAccount) {
            name.text = account.name
            number.text = account.number
            balance.text = "$%.2f".format(account.balance)
            card.onClick {
                EventBus.getDefault().post(AccountDetailEvent(account))
            }
        }
    }
}