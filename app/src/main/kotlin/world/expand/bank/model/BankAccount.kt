package world.expand.bank.model

data class BankAccount(val id: Long,
                       var name: String,
                       val number: String,
                       var balance: Float) {
    constructor(): this(0, "", "", 0f)
}