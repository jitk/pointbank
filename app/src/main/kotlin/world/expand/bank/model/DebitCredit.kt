package world.expand.bank.model

enum class DebitCredit {
    DEBIT, CREDIT
}