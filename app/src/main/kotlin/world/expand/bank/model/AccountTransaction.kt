package world.expand.bank.model

data class AccountTransaction(val amount: Float,
                              val debitCredit: DebitCredit,
                              val fromAccountNumber: String?,
                              val toAccountNumber: String?)
