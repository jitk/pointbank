package world.expand.bank.dialog

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.*
import world.expand.bank.R
import world.expand.bank.model.AccountTransaction
import world.expand.bank.model.BankAccount
import world.expand.bank.ui.AccountDetailsUi

class AccountDetailsFragment : DialogFragment(), AnkoLogger {
    lateinit private var ui: AccountDetailsUi
    private val transactionList: List<AccountTransaction> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = AccountDetailsUi(transactionList)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.window.attributes.windowAnimations = R.style.BankPopupDialog
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        ui.account = arguments?.getSerializable("account") as BankAccount
        return ui.createView(AnkoContext.Companion.create(ctx, this))
    }

    override fun onResume() {
        super.onResume()
        setCustomDialogSize()
    }

    private fun setCustomDialogSize() {
        dialog.window.setLayout(matchParent, matchParent)
        dialog.window.attributes.horizontalMargin = dip(20).toFloat()
        dialog.window.attributes.verticalMargin = dip(20).toFloat()
    }
}