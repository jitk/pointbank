package world.expand.bank.util

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.*
import android.graphics.drawable.shapes.OvalShape

object Drawables {
    fun oval(width: Int, height: Int, colour: Int = Color.DKGRAY) = ShapeDrawable(OvalShape()).apply {
        paint.color = colour
        intrinsicWidth = width
        intrinsicHeight = height
    }

    fun circle(radius: Int, colour: Int = Color.DKGRAY) = oval(radius, radius, colour)

    fun border(drawable: ShapeDrawable, colour: Int = Color.BLACK, width: Int = 5): Drawable {
        val border = GradientDrawable()
        border.shape = if (drawable.shape is OvalShape) GradientDrawable.OVAL else GradientDrawable.RECTANGLE
        border.color = ColorStateList.valueOf(Color.TRANSPARENT)
        border.setStroke(width, colour)
        return LayerDrawable(arrayOf(drawable, border))
    }
}