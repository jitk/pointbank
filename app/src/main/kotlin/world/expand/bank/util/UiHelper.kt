package world.expand.bank.util

import android.graphics.PorterDuff
import android.support.v4.app.Fragment
import android.widget.Button
import android.widget.EditText
import org.jetbrains.anko.hintTextColor
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor

object UiHelper {
    fun EditText.styleText(size: Float, colour: Int, hintColour: Int? = null) {
        this.textSize = size
        this.textColor = colour
        if (hintColour != null) {
            this.hintTextColor = hintColour
            this.background.setColorFilter(hintColour, PorterDuff.Mode.SRC_IN)
        }
    }

    fun Button.styleButton(size: Float, textColour: Int, padding: Int) {
        this.textSize = size
        this.textColor = textColour
        this.padding = padding
    }

    fun Fragment.hideKeyboard() {
        view?.context?.inputMethodManager?.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}