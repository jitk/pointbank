package world.expand.bank.page

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.alert
import org.jetbrains.anko.support.v4.act
import world.expand.bank.BankActivity
import world.expand.bank.service.UserService
import world.expand.bank.ui.LoginUi
import world.expand.bank.util.UiHelper.hideKeyboard

class LoginFragment : Fragment(), AnkoLogger {
    var prefs: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        prefs = (act as BankActivity).prefs
        return LoginUi().createView(AnkoContext.create(context, this))
    }

    internal fun loginWithFirebase(email: String?, password: String?, rememberMe: Boolean) {
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            act.alert("Email and Password must be supplied", "Invalid Credentials")
        } else {
            hideKeyboard()
            UserService.doLogin(act as BankActivity, email!!, password!!, rememberMe)
        }
    }
}