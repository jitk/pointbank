package world.expand.bank.page

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.crash.FirebaseCrash
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.EventBusException
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast
import world.expand.bank.BankActivity
import world.expand.bank.data.FirebaseDb
import world.expand.bank.event.LoginEvent
import world.expand.bank.event.LogoutEvent
import world.expand.bank.model.BankAccount
import world.expand.bank.ui.AccountListUi

class AccountListFragment : Fragment(), AnkoLogger {
    private val accounts: MutableList<BankAccount> = mutableListOf()
    private val keys: MutableList<String> = mutableListOf()
    lateinit private var accountAdapter: AccountListUi.AccountListAdapter
    lateinit private var ui: AccountListUi

    private var accountListener: ChildEventListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = AccountListUi(accounts)
        accountAdapter = ui.accountAdapter
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return ui.createView(AnkoContext.create(context, this))
    }

    fun initialiseDataConnection() {
        accountListener = FirebaseDb.trackData("accounts",
                changeHandler = { key, account ->
                    (act as BankActivity).setLoading(false)

                    val idx = keys.indexOf(key)
                    if (idx != -1) {
                        accounts[idx].balance = account.balance
                        accounts[idx].name = account.name
                        onUiThread {
                            accountAdapter.notifyItemChanged(idx)
                        }
                    }
                },
                addHandler = { key, account ->
                    (act as BankActivity).setLoading(false)

                    if (!keys.contains(key)) {
                        accounts.add(account)
                        keys.add(key)
                        onUiThread {
                            accountAdapter.notifyItemInserted(accounts.size - 1)
                        }
                    }
                },
                removeHandler = { key, account ->
                    (act as BankActivity).setLoading(false)

                    val idx = keys.indexOf(key)
                    if (idx != -1) {
                        accounts.removeAt(idx)
                        keys.removeAt(idx)
                        onUiThread {
                            accountAdapter.notifyItemRemoved(idx)
                        }
                    }
                },
                cancelHandler = { error ->
                    (act as BankActivity).setLoading(false)

                    FirebaseCrash.report(error.toException())
                    warn { "Failed to query database: $error" }
                })
    }

    override fun onStop() {
        super.onStop()
        if (accountListener != null) FirebaseDb.remove(accountListener!!)
    }

    override fun onStart() {
        super.onStart()
        initialiseDataConnection()
    }
}