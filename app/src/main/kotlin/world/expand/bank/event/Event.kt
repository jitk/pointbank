package world.expand.bank.event

import world.expand.bank.model.User
import world.expand.bank.model.BankAccount

data class LoginEvent(val user: User)
data class LogoutEvent(val user: User? = null)

data class AccountDetailEvent(val account: BankAccount)

