package world.expand.bank.data

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import world.expand.bank.model.BankAccount

object FirebaseDb {
    // No-Op handler allows selective registration of callbacks
    val NO_OP_HANDLER = { s:String, b:BankAccount -> }

    fun remove(listener: ChildEventListener) {
        FirebaseDatabase.getInstance().reference.removeEventListener(listener)
    }

    fun trackData(
            reference: String,
            cancelHandler: (DatabaseError) -> Unit,
            addHandler: (String, BankAccount) -> Unit = NO_OP_HANDLER,
            changeHandler: (String, BankAccount) -> Unit = NO_OP_HANDLER,
            removeHandler: (String, BankAccount) -> Unit = NO_OP_HANDLER,
            moveHandler: (String, BankAccount) -> Unit = NO_OP_HANDLER
    ): ChildEventListener {
        val listener = object : ChildEventListener {
            override fun onChildMoved(accountSnap: DataSnapshot, s: String?) {
                moveHandler(accountSnap.key, accountSnap.getValue(BankAccount::class.java))
            }

            override fun onChildChanged(accountSnap: DataSnapshot, s: String?) {
                changeHandler(accountSnap.key, accountSnap.getValue(BankAccount::class.java))
            }

            override fun onChildAdded(accountSnap: DataSnapshot, s: String?) {
                addHandler(accountSnap.key, accountSnap.getValue(BankAccount::class.java))
            }

            override fun onChildRemoved(accountSnap: DataSnapshot) {
                removeHandler(accountSnap.key, accountSnap.getValue(BankAccount::class.java))
            }

            override fun onCancelled(error: DatabaseError) {
                cancelHandler(error)
            }
        }
        FirebaseDatabase.getInstance().reference
                .child(reference)
                .addChildEventListener(listener)

        return listener
    }
}